package fr.epsi.superdatecalculator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.epsi.superdatecalculator.model.Celebrity;
import fr.epsi.superdatecalculator.model.CelebrityListModel;

class TestCelebrityListModel {

    // Une liste vide doit avoir 0 éléments
	@Test
	void test_zero_elements() {
		CelebrityListModel clm = new CelebrityListModel();
		assertEquals(0, clm.getSize());
	}
	
	// Ajouter un élément dans la liste doit:
	// - augmenter sa taille de 1
	// - rendre l'élément disponible depuis la liste
	@Test
	void test_add_element() {
		Celebrity c = new Celebrity("a", 1055);
		CelebrityListModel clm = new CelebrityListModel();
		clm.add(c);
		
		assertEquals(1, clm.getSize());
		assertEquals(c, clm.getElementAt(0));
	}
	
    // Une liste qu'on "clear" doit devenir vide
    @Test
    void test_clear_list() {
        CelebrityListModel clm = new CelebrityListModel();
        clm.add(new Celebrity("a"));
        clm.add(new Celebrity("b"));
        
        assertEquals(2, clm.getSize());
        
        clm.clear();
        
        assertEquals(0, clm.getSize());
    }
	
	// Le tri de la liste par dates croissantes doit fonctionner
	@Test
	void test_sort_growing_date() {
		Celebrity a = new Celebrity("a", 922);
		Celebrity b = new Celebrity("b", 1024);
		Celebrity c = new Celebrity("c", 744);
		CelebrityListModel clm = new CelebrityListModel();
		clm.add(a);
		clm.add(b);
		clm.add(c);
		
		clm.sortByBirthDate();

		assertEquals(c, clm.getElementAt(0));
		assertEquals(a, clm.getElementAt(1));
		assertEquals(b, clm.getElementAt(2));
	}

    // Le tri de la liste par ordre alphabétique des noms doit fonctionner
	@Test
	void test_sort_growing_names() {
		Celebrity b = new Celebrity("b", 922);
		Celebrity c = new Celebrity("c", 1024);
		Celebrity a = new Celebrity("a", 1890);
		CelebrityListModel clm = new CelebrityListModel();
		clm.add(b);
		clm.add(c);
		clm.add(a);
		
		clm.sortByName();

		assertEquals(a, clm.getElementAt(0));
		assertEquals(b, clm.getElementAt(1));
		assertEquals(c, clm.getElementAt(2));
	}
}
