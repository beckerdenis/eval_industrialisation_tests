package fr.epsi.superdatecalculator;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;

import fr.epsi.superdatecalculator.controller.DateCalculator;

class TestDateCalculator {
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

	// On doit pouvoir lire le numéro du jour et du mois à partir d'une date
	@Test
	void test_day_month() throws ParseException {
		DateCalculator dc = new DateCalculator(sdf.parse("15-07-2012"));
		
		assertEquals(15, dc.getDayOfMonth());
		assertEquals(7, dc.getMonthOfYear());
	}
	
	// Le nombre de jours/mois/années d'écart entre "aujourd'hui" et "aujourd'hui" doit être 0
	@Test
	void test_elapsed_from_today() {
		DateCalculator dc = new DateCalculator(new Date()); // new Date() = "aujourd'hui"

		assertEquals(0, dc.getDaysOld());
		assertEquals(0, dc.getMonthsOld());
		assertEquals(0, dc.getYearsOld());
	}

	// On doit pouvoir lire l'année d'une date
	@Test
	void test_get_year() throws ParseException {
		DateCalculator dc = new DateCalculator(sdf.parse("01-05-1998"));

		assertEquals(1998, dc.getYear());
	}

	// On peut dire si une année est bissextile ou non
	@Test
	void test_leap_year() throws ParseException {
		// not multiple of 4
		assertFalse(new DateCalculator(sdf.parse("05-05-1995")).isLeap());
		assertFalse(new DateCalculator(sdf.parse("05-05-1994")).isLeap());
		assertFalse(new DateCalculator(sdf.parse("05-05-1993")).isLeap());
		assertFalse(new DateCalculator(sdf.parse("05-05-1991")).isLeap());
		
		// multiple of 4 but not 100
		assertTrue(new DateCalculator(sdf.parse("05-05-1992")).isLeap());
		assertTrue(new DateCalculator(sdf.parse("05-05-1996")).isLeap());
		
		// multiple of 4 and 100 but not 400
		assertFalse(new DateCalculator(sdf.parse("05-05-1800")).isLeap());
		assertFalse(new DateCalculator(sdf.parse("05-05-1900")).isLeap());
		
		// multiple of 400
		assertTrue(new DateCalculator(sdf.parse("05-05-1600")).isLeap());
		assertTrue(new DateCalculator(sdf.parse("05-05-2000")).isLeap());
	}
	
	// On peut lire le nom du jour et du mois d'une date donnée
	@Test
	void test_french_day_name_and_month() throws ParseException {
		DateCalculator dc;
		
		dc = new DateCalculator(sdf.parse("01-10-2019"));
		assertEquals("mardi", dc.getFrenchDayName());
		assertEquals("octobre", dc.getFrenchMonthName());

		dc = new DateCalculator(sdf.parse("31-01-2019"));
		assertEquals("jeudi", dc.getFrenchDayName());
		assertEquals("janvier", dc.getFrenchMonthName());
	}
}
