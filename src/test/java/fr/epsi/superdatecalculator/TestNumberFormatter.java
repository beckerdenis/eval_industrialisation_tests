package fr.epsi.superdatecalculator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.epsi.superdatecalculator.controller.NumberFormatter;

class TestNumberFormatter {

    // NumberFormatter envoie une exception pour des nombre négatifs ou plus grand que 99
	@Test
	void test_below_0_or_over_99() {
		assertThrows(Exception.class, () -> NumberFormatter.formatTwoDigits(-1));
		assertThrows(Exception.class, () -> NumberFormatter.formatTwoDigits(100));
	}

	// NumberFormatter fonctionne pour des numéros à 1 chiffre (ajoute un zéro)
	@Test
	void test_0_and_9() {
		assertEquals("00", NumberFormatter.formatTwoDigits(0));
		assertEquals("09", NumberFormatter.formatTwoDigits(9));
	}

    // NumberFormatter fonctionne pour des numéros à 2 chiffres (pas de zéro ajouté)
	@Test
	void test_10_and_99() {
		assertEquals("10", NumberFormatter.formatTwoDigits(10));
		assertEquals("99", NumberFormatter.formatTwoDigits(99));
	}
}
