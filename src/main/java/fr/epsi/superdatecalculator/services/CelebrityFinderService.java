package fr.epsi.superdatecalculator.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import fr.epsi.superdatecalculator.controller.NumberFormatter;
import fr.epsi.superdatecalculator.model.Celebrity;

public class CelebrityFinderService {

	private static final String BASE_URL = "https://anniversaire-celebrite.com/ok,annee,%s,%s.html";
	
	private static String getURL(int month, int day) {
		return String.format(BASE_URL,
				NumberFormatter.formatTwoDigits(month),
				NumberFormatter.formatTwoDigits(day));
	}
	
	/**
	 * Get a list of celebrities born with the given month and day in year.
	 * 
	 * Uses an Internet connection to query results: this function takes some time to return.
	 * 
	 * @param month Month in year (1-12)
	 * @param day Day in month (1-31)
	 * @return A list of celebrities born the given month and day
	 */
	public static List<Celebrity> getListFor(int month, int day) {
		try {
			List<Celebrity> list = new ArrayList<>();
			String url = getURL(month, day);
			Document doc = Jsoup.connect(url).get();
			List<Element> guys = doc.getElementsByClass("columns").get(3).children();
			guys.forEach((guy) -> {
				String name = guy.getElementsByClass("celnom").get(0).text();
				String yearString = guy.getElementsByClass("celannee").get(0).text().trim().substring(0, 4);
				int year = Integer.valueOf(yearString);
				list.add(new Celebrity(name, year));
			});
			return list;
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}
}
