package fr.epsi.superdatecalculator.controller;

import java.time.YearMonth;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class DateCalculator {

	private final Date today = new Date();
	private final Date date;

	/**
	 * Create a DateCalculator object for the given date.
	 * 
	 * @param date Date
	 */
	public DateCalculator(Date date) {
		this.date = date;
	}
	
	/**
	 * Get the number of days elapsed since this date.
	 * 
	 * @return Number of days elapsed since this date
	 */
	public int getDaysOld() {
		long a = TimeUnit.DAYS.convert(date.getTime(), TimeUnit.MILLISECONDS);
		long b = TimeUnit.DAYS.convert(today.getTime(), TimeUnit.MILLISECONDS);
		
		return (int) (b - a);
	}

	/**
	 * Get the number of months elapsed since this date.
	 * 
	 * @return Number of months elapsed since this date
	 */
	public int getMonthsOld() {
		YearMonth a = YearMonth.from(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		YearMonth b = YearMonth.from(today.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		
		return ((int) a.until(b, ChronoUnit.MONTHS));
	}

	/**
	 * Get the number of years elapsed since this date.
	 * 
	 * @return Number of years elapsed since this date
	 */
	public int getYearsOld() {
		YearMonth a = YearMonth.from(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		YearMonth b = YearMonth.from(today.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		
		return ((int) a.until(b, ChronoUnit.YEARS));
	}
	
	/**
	 * Return this date's year.
	 * 
	 * @return This date's year
	 */
	public int getYear() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}
	
	/**
	 * Tell if the year is a leap year or not.
	 * 
	 * @return True if and only if the year is a leap year
	 */
	public boolean isLeap() {
		int year = getYear();
		return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
	}
	
	/**
	 * Return the month of this date (1-12).
	 * 
	 * @return Month of this date
	 */
	public int getMonthOfYear() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}
	
	/**
	 * Return the day number (in the month) of this date (1-31).
	 * 
	 * @return Day number of this date
	 */
	public int getDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		//calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * Return the French name of this date's day of the week.
	 * 
	 * @return French name of this date's day of the week
	 */
	public String getFrenchDayName() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		String days[] = {"dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"};
		return days[day - 1];
	}
	
	/**
	 * Return the French name of this month.
	 * 
	 * @return French name of this month
	 */
	public String getFrenchMonthName() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int month = calendar.get(Calendar.MONTH);
		String months[] = {"janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"};
		return months[month];
	}
}
