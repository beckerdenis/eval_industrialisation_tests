package fr.epsi.superdatecalculator.controller;

public class NumberFormatter {

	/**
	 * Create a String representing the given number (two digits).
	 * 
	 * Example: 0 gives "00", 1 gives "01", 2 gives "02", 30 gives "30".
	 * 
	 * Number must be between 0 and 99, otherwise an IllegalArgumentException is thrown.
	 * 
	 * @param number Number (between 0 and 99)
	 * @return String representing the number on two digits
	 */
	public static String formatTwoDigits(int number) {
		if (number > 99 || number < 0) {
			throw new IllegalArgumentException("NumberFormatter.formatTwoDigits: Number must be between 0 and 99");
		}
		if (number < 10) {
			return "0" + String.valueOf(number);
		}
		return String.valueOf(number);
	}
}
