package fr.epsi.superdatecalculator;

import fr.epsi.superdatecalculator.gui.MainFrame;

/**
 * Super date calculator.
 */
public final class App  {
	
    public static void main(String[] args) {
    	MainFrame frame = new MainFrame();
    	frame.setVisible(true);
    }
}
