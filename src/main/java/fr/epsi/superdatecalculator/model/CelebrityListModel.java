package fr.epsi.superdatecalculator.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractListModel;

public class CelebrityListModel extends AbstractListModel<Celebrity> {

	private static final long serialVersionUID = 6871329212346697351L;
	
	private List<Celebrity> list = new ArrayList<>();
	
	/**
	 * Remove all elements from this list.
	 */
	public void clear() {
		int size = list.size();
		//list.clear();
		if (size > 0) {
			fireIntervalRemoved(this, 0, size - 1);
		}
	}
	
	/**
	 * Add a celebrity to the end of this list.
	 * 
	 * @param celebrity Celebrity to add
	 */
	public void add(Celebrity celebrity) {
		list.add(celebrity);
		fireIntervalAdded(this, list.size() - 1, list.size() - 1);
	}
	
	/**
	 * Sort celebrities by growing birth years.
	 */
	public void sortByBirthDate() {
		Collections.sort(list, (c1, c2) -> {
			return c1.getBirthYear() - c2.getBirthYear();
		});
		fireContentsChanged(this, 0, list.size() - 1);
	}
	
	/**
	 * Sort celebrities by growing names (ASCII order).
	 */
	public void sortByName() {
		Collections.sort(list, (c1, c2) -> {
			return c1.getName().compareTo(c2.getName());
		});
		fireContentsChanged(this, 0, list.size() - 1);
	}
	
	/**
	 * Get this list size.
	 * 
	 * @return Number of celebrities in this list
	 */
	@Override
	public int getSize() {
		return list.size();
	}

	/**
	 * Get the celebrity at the given position.
	 * 
	 * @param index	Index to look at
	 * @return Celebrity at the given index
	 */
	@Override
	public Celebrity getElementAt(int index) {
		return list.get(index);
	}
}
