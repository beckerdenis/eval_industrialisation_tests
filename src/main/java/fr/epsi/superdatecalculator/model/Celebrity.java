package fr.epsi.superdatecalculator.model;

public class Celebrity {

	private String name;
	private Integer birthYear;
	
	/**
	 * Creates a celebrity with a name and a birth year.
	 * 
	 * @param name Name of the celebrity
	 * @param birthYear Birth year of the celebrity
	 */
	public Celebrity(String name, int birthYear) {
		this.name = name;
		this.birthYear = birthYear;
	}
	
	/**
	 * Creates a discreet celebrity with just a name.
	 * 
	 * @param name Name of the celebrity
	 */
	public Celebrity(String name) {
		this.name = name;
	}
	
	/**
	 * Get this celebrity's birth year (or null if not specified).
	 * 
	 * @return This celebrity's birth year (or null if not specified)
	 */
	public Integer getBirthYear() {
		return birthYear;
	}
	
	/**
	 * Get this celebrity's name.
	 * 
	 * @return This celebrity's name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Return a String representation of this celebrity, with its name and birth year if provided.
	 * Example: "Bob Marley (1945)" with date, or "Bob Marley" without date.
	 * 
	 * @return Representation of this celebrity in string
	 */
	@Override
	public String toString() {
		if (null != birthYear) {
			return name + " (" + birthYear + ")";
		} else {
			return name;
		}
	}
}
