package fr.epsi.superdatecalculator.gui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JFormattedTextField.AbstractFormatter;

public final class DateFormatter extends AbstractFormatter {

	private static final long serialVersionUID = 7730508024955373107L;
	
	private final String DATE_PATTERN = "dd-MM-yyyy";
	private final SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
	
	@Override
	public Object stringToValue(String text) throws ParseException {
		return formatter.parseObject(text);
	}
	
	@Override
	public String valueToString(Object value) throws ParseException {
		if (value != null && value instanceof Calendar) {
			Calendar calendar = (Calendar) value;
			return formatter.format(calendar.getTime());
		}
		return "";
	}
}
