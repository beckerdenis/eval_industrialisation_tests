package fr.epsi.superdatecalculator.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import fr.epsi.superdatecalculator.model.Celebrity;
import fr.epsi.superdatecalculator.model.CelebrityListModel;

public class ResultPanelBuilder {
	
	private JLabel sumUpLabel = new JLabel("");
	private JLabel ageLabel = new JLabel("");
	private JLabel leapYearLabel = new JLabel("");
	private JLabel dayNameLabel = new JLabel("");
	private JPanel celebrityPanel = new JPanel(new BorderLayout());
	
	private CelebrityListModel celebrityListModel = new CelebrityListModel();
	private JList<Celebrity> celebrityList = new JList<>(celebrityListModel);

	public ResultPanelBuilder() {
		JLabel celebrityLabel = new JLabel("Célébrités nées le même jour et mois :");
		celebrityLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JButton sortByBirth = new JButton("Trier par date");
		sortByBirth.addActionListener((e) -> celebrityListModel.sortByBirthDate());
		JButton sortByName = new JButton("Trier par nom");
		sortByName.addActionListener((e) -> celebrityListModel.sortByName());
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(sortByBirth);
		buttonPanel.add(sortByName);
		
		celebrityPanel.add(celebrityLabel, BorderLayout.NORTH);
		celebrityPanel.add(new JScrollPane(celebrityList), BorderLayout.CENTER);
		celebrityPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		sumUpLabel.setFont(sumUpLabel.getFont().deriveFont((float) sumUpLabel.getFont().getSize() + 4f));
	}
	
	public JPanel getPanel() {
		Component results[] = {
			sumUpLabel,
			ageLabel,
			leapYearLabel,
			dayNameLabel,
			celebrityPanel
		};
		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder("Résultats"));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		for (Component p : results) {
			panel.add(p);
		}
		return panel;
	}
	
	public void updateSumup(int day, String monthName, int year) {
		if (day == 1) {
			sumUpLabel.setText("Date choisie : " + day + "er " + monthName + " " + year);
		} else {
			sumUpLabel.setText("Date choisie : " + day + " " + monthName + " " + year);
		}
	}
	
	public void updateAge(int yearsOld, int monthsOld, int daysOld) {
		if (daysOld < 0) {
			ageLabel.setText("Si vous êtes né à cette date... vous n'êtes pas encore né.");
		} else if (daysOld == 0) {
			ageLabel.setText("Si vous êtes né à cette date... vous êtes né aujourd'hui !");
		} else if (monthsOld < 2) {
			ageLabel.setText("Si vous êtes né à cette date, vous avez " + daysOld + " jour(s).");
		} else if (monthsOld < 24) {
			ageLabel.setText("Si vous êtes né à cette date, vous avez " + monthsOld + " mois.");
		} else {
			ageLabel.setText("Si vous êtes né à cette date, vous avez " + yearsOld + " ans et vous avez vécu " + daysOld + " jours.");
		}
	}
	
	public void updateLeap(boolean isLeapYear) {
		if (isLeapYear) {
			leapYearLabel.setText("Cette année est bissextile.");
		} else {
			leapYearLabel.setText("Cette année n'est pas bissextile.");
		}
	}
	
	public void updateDayName(String dayName, int daysOld) {
		if (daysOld < 0) {
			dayNameLabel.setText("Ce sera un " + dayName + ".");
		} else if (daysOld > 0) {
			dayNameLabel.setText("C'était un " + dayName + ".");
		} else {
			dayNameLabel.setText("C'est un " + dayName + ".");
		}
	}
	
	public void updateCelebrityList(List<Celebrity> celebrityList) {
		celebrityListModel.clear();
		if (celebrityList.isEmpty()) {
			celebrityListModel.add(new Celebrity("Impossible de se connecter au site Internet"));
		} else {
			celebrityList.forEach((c) -> {
				celebrityListModel.add(c);
			});
		}
	}
	
	public void cleanUpCelebrityList() {
		celebrityListModel.clear();
		celebrityListModel.add(new Celebrity("Recherche en cours..."));
	}
}
