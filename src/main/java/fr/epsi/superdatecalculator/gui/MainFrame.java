package fr.epsi.superdatecalculator.gui;

import java.awt.BorderLayout;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import fr.epsi.superdatecalculator.controller.DateCalculator;
import fr.epsi.superdatecalculator.model.Celebrity;
import fr.epsi.superdatecalculator.services.CelebrityFinderService;

public final class MainFrame {

	private final JFrame frame = new JFrame("Super Date Calculator");

	private final UtilDateModel dateModel = new UtilDateModel();
	private final JButton okButton = new JButton("Allez");
	private final ResultPanelBuilder resultPanelBuilder = new ResultPanelBuilder();
	
	private Thread updateThread;
	private UpdateRunnable updateRunnable;
	
	public MainFrame() {
		frame.getContentPane().add(buildMainPanel());
		
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
	}
	
	public void setVisible(boolean visible) {
		frame.setVisible(visible);
	}
	
	private JPanel buildInputPanel() {
		dateModel.addChangeListener((e) -> {
			okButton.setEnabled(null != dateModel.getValue());
		});
		
		Properties prop = new Properties();
		prop.put("text.today", "Aujourd'hui");
		prop.put("text.month", "Mois");
		prop.put("text.year", "Année");
		JDatePanelImpl datePanel = new JDatePanelImpl(dateModel, prop);
		JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateFormatter());
		
		JLabel dateLabel = new JLabel("Donnez une date :");
		
		JPanel inputPanel = new JPanel();
		dateLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		inputPanel.add(dateLabel);
		inputPanel.add(datePicker);
		
		okButton.setEnabled(false);
		okButton.addActionListener((e) -> {
			if (null != updateThread && updateThread.isAlive()) {
				updateRunnable.disconnect();
			}
			
			updateRunnable = new UpdateRunnable();
			updateThread = new Thread(updateRunnable);
			updateThread.start();
		});
		
		JPanel controlPanel = new JPanel();
		controlPanel.add(okButton);
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(inputPanel, BorderLayout.CENTER);
		panel.add(controlPanel, BorderLayout.SOUTH);
		panel.setBorder(BorderFactory.createTitledBorder("Entrées"));
		return panel;
	}
	
	private JPanel buildMainPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(buildInputPanel(), BorderLayout.NORTH);
		panel.add(resultPanelBuilder.getPanel(), BorderLayout.CENTER);
		return panel;
	}
	
	private class UpdateRunnable implements Runnable {
		
		private boolean connected = true;
		
		public synchronized void disconnect() {
			connected = false;
		}
		
		public synchronized boolean isConnected() {
			return connected;
		}
		
		@Override
		public void run() {
			Date date = dateModel.getValue();
			
			DateCalculator ac = new DateCalculator(date);
			
			if (isConnected()) {
				SwingUtilities.invokeLater(() -> {
					resultPanelBuilder.updateSumup(ac.getDayOfMonth(), ac.getFrenchMonthName(), ac.getYear());
					resultPanelBuilder.updateAge(ac.getYearsOld(), ac.getMonthsOld(), ac.getDaysOld());
					resultPanelBuilder.updateLeap(ac.isLeap());
					resultPanelBuilder.updateDayName(ac.getFrenchDayName(), ac.getDaysOld());
					resultPanelBuilder.cleanUpCelebrityList();
				});
			}
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			List<Celebrity> celebrityList = CelebrityFinderService.getListFor(ac.getMonthOfYear(), ac.getDayOfMonth());
			
			if (isConnected()) {
				SwingUtilities.invokeLater(() -> resultPanelBuilder.updateCelebrityList(celebrityList));
			}
		}
	}
}
