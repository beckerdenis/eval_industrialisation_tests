1/ Importer le projet Maven

Exemple avec Eclipse :
	"File > Import > Maven > Existing Maven Projects"
	puis dans "Root Directory" donner le r�pertoire racine (celui qui contient "pom.xml") puis "Finish"

2/ Pour lancer les tests et g�n�rer les rapports, lancer la cible maven "clean site verify"

Exemple avec Eclipse :
	Clic droit sur le projet (dans la partie de gauche "Package Explorer") puis "Run as" puis "Maven build"
	Dans la fen�tre qui s'ouvre, il faut juste ajouter ceci dans Goals : "clean site verify" puis faire "Run"

3/ Les rapports (format HTML) peuvent �tre ouverts dans un navigateur web
(pas besoin d'un serveur web, il suffit d'ouvrir les fichiers)

- Pour le rapport des tests unitaires : � partir de la racine du projet c'est le fichier "target/site/surefire-report.html"
- Pour le rapport de couverture de code : � partir de la racine du projet c'est le fichier "target/site/jacoco/index.html"
  puis les packages et classes sont cliquables jusqu'au d�tail du code source.